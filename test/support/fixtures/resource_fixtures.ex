defmodule Hr.ResourceFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Hr.Resource` context.
  """

  @doc """
  Generate a employee.
  """
  def employee_fixture(attrs \\ %{}) do
    {:ok, employee} =
      attrs
      |> Enum.into(%{
        age: 42,
        gender: "some gender",
        name: "some name",
        profession: "some profession",
        salary: "120.5"
      })
      |> Hr.Resource.create_employee()

    employee
  end
end
