defmodule Hr.ResourceTest do
  use Hr.DataCase

  alias Hr.Resource

  describe "employees" do
    alias Hr.Resource.Employee

    import Hr.ResourceFixtures

    @invalid_attrs %{age: nil, gender: nil, name: nil, profession: nil, salary: nil}

    test "list_employees/0 returns all employees" do
      employee = employee_fixture()
      assert Resource.list_employees() == [employee]
    end

    test "get_employee!/1 returns the employee with given id" do
      employee = employee_fixture()
      assert Resource.get_employee!(employee.id) == employee
    end

    test "create_employee/1 with valid data creates a employee" do
      valid_attrs = %{age: 42, gender: "some gender", name: "some name", profession: "some profession", salary: "120.5"}

      assert {:ok, %Employee{} = employee} = Resource.create_employee(valid_attrs)
      assert employee.age == 42
      assert employee.gender == "some gender"
      assert employee.name == "some name"
      assert employee.profession == "some profession"
      assert employee.salary == Decimal.new("120.5")
    end

    test "create_employee/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Resource.create_employee(@invalid_attrs)
    end

    test "update_employee/2 with valid data updates the employee" do
      employee = employee_fixture()
      update_attrs = %{age: 43, gender: "some updated gender", name: "some updated name", profession: "some updated profession", salary: "456.7"}

      assert {:ok, %Employee{} = employee} = Resource.update_employee(employee, update_attrs)
      assert employee.age == 43
      assert employee.gender == "some updated gender"
      assert employee.name == "some updated name"
      assert employee.profession == "some updated profession"
      assert employee.salary == Decimal.new("456.7")
    end

    test "update_employee/2 with invalid data returns error changeset" do
      employee = employee_fixture()
      assert {:error, %Ecto.Changeset{}} = Resource.update_employee(employee, @invalid_attrs)
      assert employee == Resource.get_employee!(employee.id)
    end

    test "delete_employee/1 deletes the employee" do
      employee = employee_fixture()
      assert {:ok, %Employee{}} = Resource.delete_employee(employee)
      assert_raise Ecto.NoResultsError, fn -> Resource.get_employee!(employee.id) end
    end

    test "change_employee/1 returns a employee changeset" do
      employee = employee_fixture()
      assert %Ecto.Changeset{} = Resource.change_employee(employee)
    end
  end
end
