defmodule Hr.Repo.Migrations.AddCompanyIdToEmployees do
  use Ecto.Migration

  def change do
    alter table(:employees) do
      add :company_id, references(:companies)
    end
  end
end
