defmodule Hr.Repo.Migrations.CreateEmployees do
  use Ecto.Migration

  def change do
    create table(:employees) do
      add :name, :string
      add :age, :integer
      add :gender, :string
      add :profession, :string
      add :salary, :decimal

      timestamps()
    end
  end
end
