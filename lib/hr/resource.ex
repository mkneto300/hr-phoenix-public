defmodule Hr.Resource do
  @moduledoc """
  The Resource context.
  """

  import Ecto.Query, warn: false
  alias Hr.Repo

  alias Hr.Resource.Employee

  @doc """
  Returns the list of employees.

  ## Examples

      iex> list_employees()
      [%Employee{}, ...]

  """
  def list_employees do
    Repo.all(Employee)
  end

  @doc """
  Gets a single employee.

  Raises `Ecto.NoResultsError` if the Employee does not exist.

  ## Examples

      iex> get_employee!(123)
      %Employee{}

      iex> get_employee!(456)
      ** (Ecto.NoResultsError)

  """
  def get_employee!(id), do: Repo.get!(Employee, id)

  @doc """
  Creates a employee.

  ## Examples

      iex> create_employee(%{field: value})
      {:ok, %Employee{}}

      iex> create_employee(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_employee(attrs \\ %{}) do
    %Employee{}
    |> Employee.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a employee.

  ## Examples

      iex> update_employee(employee, %{field: new_value})
      {:ok, %Employee{}}

      iex> update_employee(employee, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_employee(%Employee{} = employee, attrs) do
    employee
    |> Employee.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a employee.

  ## Examples

      iex> delete_employee(employee)
      {:ok, %Employee{}}

      iex> delete_employee(employee)
      {:error, %Ecto.Changeset{}}

  """
  def delete_employee(%Employee{} = employee) do
    Repo.delete(employee)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking employee changes.

  ## Examples

      iex> change_employee(employee)
      %Ecto.Changeset{data: %Employee{}}

  """
  def change_employee(%Employee{} = employee, attrs \\ %{}) do
    Employee.changeset(employee, attrs)
  end

  @doc """
  Gets a single employee.

  Raises `Ecto.NoResultsError` if the Employee does not exist.

  ## Examples

      iex> by_name()
      %Employee{}

      iex> by_name()
      ** (Ecto.NoResultsError)

  """

  def by_name(name) do
    #Repo.get_by(Employee, [name: name])
    Repo.all(from e in Employee, where: e.name == ^name)
  end

  @doc """
  Returns a list of all employees names.

  Returns an empty list otherwise.

  ## Examples

      iex> nameslist()
      ["Joe Doe", "Gabriela Stewards", "Steven Seagal", "Patrick Bernt","Nathalie Sulu", "Mariah Chenco"]

      iex> nameslist()
      []

  """

  def nameslist do
    Repo.all(from e in Employee, select: e.name)
  end

  @doc """
  Returns a list of all available options for gender.

  Returns 0 if male, female or other is not found in db.

  ## Examples

      iex> genderlist()
      %{:malecount => 3, :femalecount => 4, :othercount => 2}

      iex> genderlist()
      %{:malecount => 0, :femalecount => 0, :othercount => 0}

  """

  def genderlist do
    males =
    Repo.all(from e in Employee, where: e.gender == "male")
    |> Enum.count

    females =
    Repo.all(from e in Employee, where: e.gender == "female")
    |> Enum.count

    other =
    Repo.all(from e in Employee, where: e.gender == "other")
    |> Enum.count

    %{:malecount => males, :femalecount => females, :othercount => other}
  end
end
