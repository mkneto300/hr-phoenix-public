defmodule Hr.Resource.Employee do
  use Ecto.Schema
  import Ecto.Changeset

  schema "employees" do
    field :age, :integer
    field :gender, :string
    field :name, :string
    field :profession, :string
    field :salary, :decimal
    belongs_to :company, Hr.Companies.Company

    timestamps()
  end

  @doc false
  def changeset(employee, attrs) do
    employee
    |> cast(attrs, [:name, :age, :gender, :profession, :salary, :company_id])
    |> validate_required([:name, :age, :gender, :profession, :salary, :company_id])
  end
end
