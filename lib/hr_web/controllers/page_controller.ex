defmodule HrWeb.PageController do
  use HrWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
