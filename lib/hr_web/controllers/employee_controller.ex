defmodule HrWeb.EmployeeController do
  use HrWeb, :controller

  alias Hr.Resource
  alias Hr.Companies
  alias Hr.Resource.Employee

  def index(conn, _params) do
    employees = Resource.list_employees()
    render(conn, "index.html", employees: employees)
  end

  def new(conn, _params) do
    companies = Companies.get_info_for_select
    changeset = Resource.change_employee(%Employee{})
    render(conn, "new.html", changeset: changeset, companies: companies)
  end

  def create(conn, %{"employee" => employee_params}) do
    companies = Companies.get_info_for_select
    case Resource.create_employee(employee_params) do
      {:ok, employee} ->
        conn
        |> put_flash(:info, "Employee created successfully.")
        |> redirect(to: Routes.employee_path(conn, :show, employee))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset, companies: companies)
    end
  end

  def show(conn, %{"id" => id}) do
    employee = Resource.get_employee!(id)
    render(conn, "show.html", employee: employee)
  end

  def edit(conn, %{"id" => id}) do
    employee = Resource.get_employee!(id)
    changeset = Resource.change_employee(employee)
    render(conn, "edit.html", employee: employee, changeset: changeset)
  end

  def update(conn, %{"id" => id, "employee" => employee_params}) do
    employee = Resource.get_employee!(id)

    case Resource.update_employee(employee, employee_params) do
      {:ok, employee} ->
        conn
        |> put_flash(:info, "Employee updated successfully.")
        |> redirect(to: Routes.employee_path(conn, :show, employee))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", employee: employee, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    employee = Resource.get_employee!(id)
    {:ok, _employee} = Resource.delete_employee(employee)

    conn
    |> put_flash(:info, "Employee deleted successfully.")
    |> redirect(to: Routes.employee_path(conn, :index))
  end

  def getbyname(conn, _params) do
      changeset = Resource.change_employee(%Employee{})
      render conn, "byname.html", changeset: changeset
  end

 def showbyname(conn, %{"_csrf_token" => _token, "employee" => emp} = params) do
      %{"name" => name} = emp

      employees = Resource.by_name(name)
      render conn, "showbyname.html", employees: employees
  end

  def getnameslist(conn, _params) do
    names = Resource.nameslist
    render conn, "nameslist.html", names: names
  end

  def getgenderlist(conn, _params) do
    gender_count = Resource.genderlist()
    render conn, "genderlist.html", gendercount: gender_count
  end
end
