defmodule HrWeb.EmployeeView do
  use HrWeb, :view

  alias Hr.Companies

  @doc """
  Gets a company the company name based on its id.

  Raises `Ecto.NoResultsError` if the Company does not exist but returns "N/A"

  ## Examples

      iex> get_company_name(1) ## Existing company id
      "Software Solutions CA de SV"

      iex> get_company_name(456) ## Non existing company id
      "N/A"

  """

  def get_company_name(company_id)  do
    try do
      result = Companies.get_company!(company_id)
      #{:ok, result}
      result.name
    rescue
      Ecto.NoResultsError ->
      #{:error, :not_found, "No result found"}
      "N/A"
    end
  end

  @doc """
  Returns "0" if the passed argument does not contained any value.
  Return the actual argument value otherwise.

  ## Examples

      iex> zero_if_null(1)
      1

      iex> zero_if_null("")
      "0"

  """

  def zero_if_null(val) do
    if val , do: val, else: "0";
  end
end
