defmodule HrWeb.PageView do
  use HrWeb, :view

  defp no_user() do
    render("_no_user.html")
  end

  defp get_user_menu(email) do
      case email do
        "digitador@gmail.com" -> render "_digitador_menu.html"
        "consultor@example.com" ->  render "_consultor_menu.html"
      end
  end

end
